import './products.scss';
import { useEffect, useState } from 'react';
import { Input, AutoComplete, Tag } from 'antd';
import {Category} from "../../components/category/category";
const axios = require('axios')
// const mockData = require('./ingredients_api_response.json');

// const ingredients=mockData


interface IProducts {
  resultShow: boolean;
  setResultShow: (resultShow: boolean) => void,
}

interface IsIngredient{
  value:string;
  category:string;
  id:string;
}

export const Products = ({resultShow, setResultShow} : IProducts) => {
  const [value, setValue] = useState('');
  const [options, setOptions] = useState<{ value: string, id: string }[]>([]);

  const [selectedProducts, setSelectedProducts] = useState<string[]>([]);

  const [ingredients, setIngredients] = useState<IsIngredient[]>([]);

  useEffect(() => {
axios.get('http://localhost:5000/ingredients')
.then((resp:any) => {setIngredients(resp.data)})

}, [])

  const getCategory = (value: string): string => {
    let foundCategory: string = "";
    ingredients.forEach(element => {
      if (element.value === value) {
        console.log("found: " + element.category);
        foundCategory = element.category
        return (element.category)
      };
    })
    return foundCategory;
  }

  const onSearch = (searchText: string) => {
    // console.log("search", searchText);
    setOptions(
      ingredients.filter(element => { return element.value.includes(searchText) })
    );
  };
  const onSelect = (data: string) => {
    // console.log('onSelect', data);
    setSelectedProducts([...selectedProducts, data])
  };
  const onChange = (data: string) => {
    setValue(data);
  };

  const onSubmit = () => {
    console.log("onSearch!", resultShow)
    setResultShow(!resultShow);
  }
 
  const categoryList = [
  {name:'VEGETABLE', styleName: 'tag__meat'}, 
  {name:'FRUIT', styleName: 'tag__vegetables'},
  {name:'grain', styleName: 'tag__fruits'},
  {name:'MEAT', styleName: 'tag__meat'}];

  return (
    <div className="container">
      <AutoComplete
        options={options}
        onSelect={onSelect}
        onSearch={onSearch}
        onChange={onChange}
        className="autoComplete"
      >
        <Input.Search placeholder="wpisz produkty..." />
      </AutoComplete>

      <section className="category">
        {categoryList.map(category => {
          return <Category styleName={category.styleName} categoryName={category.name} products={selectedProducts.filter( p => (category.name === getCategory(p)))} />
        })}
        

        <div className="tag">
          <h5>Mięso</h5>
          <Tag className="tag__container tag__meat" closable={true}>Kurczak</Tag>
          <Tag className="tag__container tag__meat" closable={true}>Cielęcina</Tag>
          {selectedProducts?.map((i, index) => {
            return getCategory(i) === "MEAT" && <Tag className="tag__container tag__meat" key={index} closable={true}>{i}</Tag>
          })}
        </div>

        <div className="tag">
          <h5>Warzywa</h5>
          <Tag className="tag__container tag__vegetables" closable={true}>Rzodkiewka</Tag>
          <Tag className="tag__container tag__vegetables" closable={true}>Sałata</Tag>
          {selectedProducts?.map((i, index) => {
            return getCategory(i) === "VEGETABLE" && <Tag className="tag__container tag__vegetables" key={index} closable={true}>{i}</Tag>
          })}
        </div>

        <div className="tag">
          <h5>Owoce</h5>
          <Tag className="tag__container tag__fruits" closable={true}>Jagody</Tag>
          <Tag className="tag__container tag__fruits" closable={true}>Pomarańcze</Tag>
          {selectedProducts?.map((i, index) => {
            return getCategory(i) === "FRUIT" && <Tag className="tag__container tag__fruits" key={index} closable={true}>{i}</Tag>
          })}
        </div>
        <div className="tag">
          <h5>Zbożowe</h5>
          <Tag className="tag__container tag__grain" closable={true}>Pieczywo</Tag>
          <Tag className="tag__container tag__grain" closable={true}>Płatki owsiane</Tag>
          {selectedProducts?.map((i, index) => {
            return getCategory(i) === "grain" && <Tag className="tag__container tag__grain" key={index} closable={true}>{i}</Tag>
          })}
        </div>

        <div className="tag">
          <h5>Przyprawy i dodatki</h5>
          <Tag className="tag__container tag__additives" closable={true}>Miód</Tag>
          <Tag className="tag__container tag__additives" closable={true}>Musztarda</Tag>
          {selectedProducts?.map((i, index) => {
            return getCategory(i) === "additives" && <Tag className="tag__container tag__additives" key={index} closable={true}>{i}</Tag>
          })}
        </div>
      </section>
      <section className="button-container">
        <button className="button__search" onClick={()=>{onSubmit()}}>Szukaj</button>
      </section>

    </div>
  )
}
