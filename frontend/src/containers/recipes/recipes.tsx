import './recipes.scss';
import logo from '../../img/zcm_mini.png';

export const Recipes = () => {
  const resultList = [
    { url: "https://www.kwestiasmaku.com/przepis/losos-pieczony-na-ryzu", percent: 100, missing: 3 },
    { url: "https://www.kwestiasmaku.com/przepis/losos-pieczony-na-ryzu", percent: 30, missing: 3 },
    { url: "https://www.kwestiasmaku.com/przepis/losos-pieczony-na-ryzu", percent: 30, missing: 3 },
  ];

  return (
    <div className="recipes">
      <img src={logo} alt="Mini Logo" />
      <header className="recipes__header">
        <h2>Przepisy</h2>
      </header>
      <section className="recipes__section">
        <header>
          <h4>Kwestia Smaku</h4>
        </header>
        {resultList.map((item, index) => {
          return <div key={index}>
            <div className="result">
              <a href={item.url} target="__blank">{item.url}</a>
              <div className="result__percent">{item.percent}%</div>
            </div>
          </div>
        })}
      </section>

    </div>
  );
}