import { Tag } from 'antd';

interface ICategory {
    categoryName: string;
    products: Array<string>;
    styleName: string;
}
export const Category = ({categoryName, products, styleName} : ICategory) => {
    return <div className="tag">
    <h5>{categoryName}</h5>

    {products.map((product, index) => {
        return <Tag key={index} className={`tag__container ${styleName}`} closable={true}>{product}</Tag>
    })}
  </div>
}