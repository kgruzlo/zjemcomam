import './App.scss';
import { useState } from 'react';
import { Recipes } from './containers/recipes/recipes';
import { Products } from './containers/products/products';
import { Header } from './containers/header/header';

function App() {
  const [resultShow, setResultShow] = useState<boolean>(false);

  return (
    <div className={`App ${ resultShow ? "background__left" : "background__right"}` }>
      <main className="main">
        {!resultShow && <header className="main__header"><Header /></header>}
        <article className="main__products">
          <Products resultShow={resultShow} setResultShow={setResultShow} />
        </article>

        {resultShow && <article className="main__recipes"><Recipes /></article>}

      </main>
      {/* <aside></aside> */}
    </div>
  );
}

export default App;
